<?php
session_start();
include 'connectionDB.php';
    if (isset($_POST['email']) && isset($_POST['password'])){

        $email = $_POST['email'];
        $pass = $_POST['password'];
        
        if (empty($email)){
            // header("Location: login.php?error=Email is required");
        }else if (empty($pass)){
            // header("Location: login.php?error=Password is required");
        }else{
            $stmt = $conn->prepare("SELECT * FROM users WHERE email=?");
            $stmt->execute([$email]);
            if ($stmt->rowCount() === 1){
                $user = $stmt->fetch();
                
                $user_id = $user['id'];
                $user_email = $user['email'];
                $user_password = $user['password'];
                $user_first_name = $user['first_name'];
                $user_user_type = $user['user_type'];
                $user_profile_image = $user['profile_image'];
                if ($email === $user_email){
                    if (password_verify($pass, $user_password)){
                        $_SESSION['user_id'] = $user_id;
                        $_SESSION['user_email'] = $user_email;
                        $_SESSION['user_first_name'] =  $user_first_name;
                        $_SESSION['user_user_type'] =  $user_user_type;
                        $_SESSION['user_profile_image'] =  $user_profile_image;
                    }else{
                        header("Location: login.php?error=<i class='fas fa-exclamation-triangle' style='font-size:14px'></i> The password you've entered is incorrect");
                    }
                }else {
                    header("Location: login.php?error=<i class='fas fa-exclamation-triangle' style='font-size:14px'></i> The email you entered is not connected to the system");
                }
            }else{
                header("Location: login.php?error=<i class='fas fa-exclamation-triangle' style='font-size:14px'></i> The email you entered is not connected to the system");
            }
        } 
    }
?> 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,400,0,0" />
        <!-- <link rel="stylesheet" type="text/css" href="../TAG-S-WATER-PURIFIED-DRINKING-WATER/CSS/Dashboard.css"> -->
        <link href="http://fonts.cdnfonts.com/css/cocogoose" rel="stylesheet">
        <link href="http://fonts.cdnfonts.com/css/phantom-2" rel="stylesheet">
        <link href="http://fonts.cdnfonts.com/css/switzer" rel="stylesheet">
        <link href="http://fonts.cdnfonts.com/css/outfit" rel="stylesheet">
        <link href="http://fonts.cdnfonts.com/css/galhau-display" rel="stylesheet">
        <link href="http://fonts.cdnfonts.com/css/malberg-trial" rel="stylesheet">
        <link href="https://fonts.cdnfonts.com/css/rajdhani" rel="stylesheet">
        <title>Tag's Water Purified Drinking Water</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" charset="utf-8"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
        <!-- <script src="./index.js"></script> -->
    </head>
    <body>
    
        <div class="container">
        <div class="menu">
        <div class="menu-btn">
        <i class="fas fa-bars"></i>
        </div>
   <div class="side-bar">
        <div class="close-btn">
        <i class="fas fa-times"></i>
        </div>
        <div class="menu">
        <div class="title">
            <div class="titlelogo">
                <img class="tagslogo" src="../Tag-s-Water-Purified-Drinking-Water/Pictures and Icons/tags logo.png" >
            </div>
            <div class="close" id="close-btn" onclick="myFunctionhp(this)">
                <svg xmlns="http://www.w3.org/2000/svg" height="24" width="24"><path d="M6.4 19 5 17.6l5.6-5.6L5 6.4 6.4 5l5.6 5.6L17.6 5 19 6.4 13.4 12l5.6 5.6-1.4 1.4-5.6-5.6Z"/></svg>
            </div>
        </div>
        <div id="dashboard" class="item"><a href="Dashboard.php"><svg xmlns="http://www.w3.org/2000/svg" height="24" width="24"><path d="M13 9V3h8v6ZM3 13V3h8v10Zm10 8V11h8v10ZM3 21v-6h8v6Z"/></svg>
        DASHBOARD</a></div>
        <div id="pointofsales" class="item"><a href="Pointofsales.php"><svg xmlns="http://www.w3.org/2000/svg" height="24" width="24"><path d="M7 8q-.825 0-1.412-.588Q5 6.825 5 6V4q0-.825.588-1.413Q6.175 2 7 2h10q.825 0 1.413.587Q19 3.175 19 4v2q0 .825-.587 1.412Q17.825 8 17 8Zm0-2h10V4H7v2ZM4 22q-.825 0-1.412-.587Q2 20.825 2 20v-1h20v1q0 .825-.587 1.413Q20.825 22 20 22Zm-2-4 3.475-7.825q.25-.55.738-.863Q6.7 9 7.3 9h9.4q.6 0 1.088.312.487.313.737.863L22 18Zm6.5-2h1q.2 0 .35-.15.15-.15.15-.35 0-.2-.15-.35Q9.7 15 9.5 15h-1q-.2 0-.35.15-.15.15-.15.35 0 .2.15.35.15.15.35.15Zm0-2h1q.2 0 .35-.15.15-.15.15-.35 0-.2-.15-.35Q9.7 13 9.5 13h-1q-.2 0-.35.15-.15.15-.15.35 0 .2.15.35.15.15.35.15Zm0-2h1q.2 0 .35-.15.15-.15.15-.35 0-.2-.15-.35Q9.7 11 9.5 11h-1q-.2 0-.35.15-.15.15-.15.35 0 .2.15.35.15.15.35.15Zm3 4h1q.2 0 .35-.15.15-.15.15-.35 0-.2-.15-.35-.15-.15-.35-.15h-1q-.2 0-.35.15-.15.15-.15.35 0 .2.15.35.15.15.35.15Zm0-2h1q.2 0 .35-.15.15-.15.15-.35 0-.2-.15-.35-.15-.15-.35-.15h-1q-.2 0-.35.15-.15.15-.15.35 0 .2.15.35.15.15.35.15Zm0-2h1q.2 0 .35-.15.15-.15.15-.35 0-.2-.15-.35-.15-.15-.35-.15h-1q-.2 0-.35.15-.15.15-.15.35 0 .2.15.35.15.15.35.15Zm3 4h1q.2 0 .35-.15.15-.15.15-.35 0-.2-.15-.35-.15-.15-.35-.15h-1q-.2 0-.35.15-.15.15-.15.35 0 .2.15.35.15.15.35.15Zm0-2h1q.2 0 .35-.15.15-.15.15-.35 0-.2-.15-.35-.15-.15-.35-.15h-1q-.2 0-.35.15-.15.15-.15.35 0 .2.15.35.15.15.35.15Zm0-2h1q.2 0 .35-.15.15-.15.15-.35 0-.2-.15-.35-.15-.15-.35-.15h-1q-.2 0-.35.15-.15.15-.15.35 0 .2.15.35.15.15.35.15Z"/></svg>
        POINT OF SALES</a></div>
        <div id="reports" class="item"><a class="sub-btn"><svg xmlns="http://www.w3.org/2000/svg" height="24" width="24"><path d="M2 21V3h10v4h10v14Zm2-2h6v-2H4Zm0-4h6v-2H4Zm0-4h6V9H4Zm0-4h6V5H4Zm8 12h8V9h-8Zm2-6v-2h4v2Zm0 4v-2h4v2Z"/></svg>
        REPORTS<i class="fas fa-angle-right dropdown"></i></a>
            <div class="sub-menu">
                <a href="Reports-sales.php" class="sub-item">Sales</a>
                <a href="Reports-deliverywalkin.php" class="sub-item">Delivery/Walk-in</a>
                <a href="Reports-datalogs.php" class="sub-item">Data Logs</a>
                <a href="Reports-inventory.php" class="sub-item">Inventory</a>
                <a href="Reports-itemissue.php" class="sub-item">Item Issue</a>
                <a href="Reports-inventory.php" class="sub-item">Attendance</a>
                <a href="Reports-expense.php" class="sub-item">Expense</a>
            </div>
        </div>
        <div id="monitoring" class="item"><a class="sub-btn"><svg xmlns="http://www.w3.org/2000/svg" height="24" width="24"><path d="M3 21v-2l2-2v4Zm4 0v-6l2-2v8Zm4 0v-8l2 2.025V21Zm4 0v-5.975l2-2V21Zm4 0V11l2-2v12ZM3 15.825V13l7-7 4 4 7-7v2.825l-7 7-4-4Z"/></svg>
        MONITORING<i class="fas fa-angle-right dropdown"></i></a>
            <div class="sub-menu">
                <a href="Monitoring-deliverypickup.php" class="sub-item">Delivery/Pick Up</a>
                <a href="Monitoring-returncontainer.php" class="sub-item">Return Container</a>
                <a href="Monitoring-customerbalance.php" class="sub-item">Customer Balance</a>
                <a href="Monitoring-scheduling.php" class="sub-item">Scheduling</a>
                <a href="Monitoring-itemhistory.php" class="sub-item">Item History</a>
            </div>
        </div>
        <div id="customer" class="item"><a href="Customer.php"><svg xmlns="http://www.w3.org/2000/svg" height="24" width="24"><path d="M1 20v-2.8q0-.85.438-1.563.437-.712 1.162-1.087 1.55-.775 3.15-1.163Q7.35 13 9 13t3.25.387q1.6.388 3.15 1.163.725.375 1.162 1.087Q17 16.35 17 17.2V20Zm18 0v-3q0-1.1-.612-2.113-.613-1.012-1.738-1.737 1.275.15 2.4.512 1.125.363 2.1.888.9.5 1.375 1.112Q23 16.275 23 17v3ZM9 12q-1.65 0-2.825-1.175Q5 9.65 5 8q0-1.65 1.175-2.825Q7.35 4 9 4q1.65 0 2.825 1.175Q13 6.35 13 8q0 1.65-1.175 2.825Q10.65 12 9 12Zm10-4q0 1.65-1.175 2.825Q16.65 12 15 12q-.275 0-.7-.062-.425-.063-.7-.138.675-.8 1.037-1.775Q15 9.05 15 8q0-1.05-.363-2.025Q14.275 5 13.6 4.2q.35-.125.7-.163Q14.65 4 15 4q1.65 0 2.825 1.175Q19 6.35 19 8ZM3 18h12v-.8q0-.275-.137-.5-.138-.225-.363-.35-1.35-.675-2.725-1.013Q10.4 15 9 15t-2.775.337Q4.85 15.675 3.5 16.35q-.225.125-.362.35-.138.225-.138.5Zm6-8q.825 0 1.413-.588Q11 8.825 11 8t-.587-1.412Q9.825 6 9 6q-.825 0-1.412.588Q7 7.175 7 8t.588 1.412Q8.175 10 9 10Zm0 8ZM9 8Z"/></svg>
        CUSTOMER</a></div>
        <div id="inventory" class="item"><a class="sub-btn"> <svg xmlns="http://www.w3.org/2000/svg" height="24" width="24"><path d="M11 21H5q-.825 0-1.413-.587Q3 19.825 3 19V5q0-.825.587-1.413Q4.175 3 5 3h4.175q.275-.875 1.075-1.438Q11.05 1 12 1q1 0 1.788.562.787.563 1.062 1.438H19q.825 0 1.413.587Q21 4.175 21 5v5h-2V5h-2v3H7V5H5v14h6Zm4.5-1.075-4.25-4.25 1.4-1.4 2.85 2.85 5.65-5.65 1.4 1.4ZM12 5q.425 0 .713-.288Q13 4.425 13 4t-.287-.713Q12.425 3 12 3t-.712.287Q11 3.575 11 4t.288.712Q11.575 5 12 5Z"/></svg>
        INVENTORY<i class="fas fa-angle-right dropdown"></i></a>
            <div class="sub-menu">
                <a href="Inventory-stocks.php" class="sub-item" id="inventory-stocks">Stocks</a>
                <a href="Inventory-details.php" class="sub-item" id="inventory-details">Item</a>
            </div>
        </div>
        <div id="employee" class="item"><a class="sub-btn"><svg xmlns="http://www.w3.org/2000/svg" height="24" width="24"><path d="M4 22q-.825 0-1.412-.587Q2 20.825 2 20V9q0-.825.588-1.413Q3.175 7 4 7h5V4q0-.825.588-1.413Q10.175 2 11 2h2q.825 0 1.413.587Q15 3.175 15 4v3h5q.825 0 1.413.587Q22 8.175 22 9v11q0 .825-.587 1.413Q20.825 22 20 22Zm2-4h6v-.45q0-.425-.238-.788-.237-.362-.662-.562-.5-.225-1.012-.337Q9.575 15.75 9 15.75q-.575 0-1.087.113-.513.112-1.013.337-.425.2-.662.562Q6 17.125 6 17.55Zm8-1.5h4V15h-4ZM9 15q.625 0 1.062-.438.438-.437.438-1.062t-.438-1.062Q9.625 12 9 12t-1.062.438Q7.5 12.875 7.5 13.5t.438 1.062Q8.375 15 9 15Zm5-1.5h4V12h-4ZM11 9h2V4h-2Z"/></svg>
        EMPLOYEE<i class="fas fa-angle-right dropdown"></i></a>
            <div class="sub-menu">
                <a href="Employee-attendance.php" class="sub-item" id="employee-attendance" >Attendance</a>
                <a href="Employee-details.php" class="sub-item" id="employee-details">Employee Details</a>
                <a href="Employee-payroll.php" class="sub-item" id="employee-payroll">Payroll</a>
            </div>
        </div>
        <div id="expense" class="item"><a class="sub-btn"><svg xmlns="http://www.w3.org/2000/svg" height="24" width="24"><path d="M3 20q-.825 0-1.412-.587Q1 18.825 1 18V7h2v11h17v2Zm4-4q-.825 0-1.412-.588Q5 14.825 5 14V6q0-.825.588-1.412Q6.175 4 7 4h14q.825 0 1.413.588Q23 5.175 23 6v8q0 .825-.587 1.412Q21.825 16 21 16Zm2-2q0-.825-.588-1.413Q7.825 12 7 12v2Zm10 0h2v-2q-.825 0-1.413.587Q19 13.175 19 14Zm-5-1q1.25 0 2.125-.875T17 10q0-1.25-.875-2.125T14 7q-1.25 0-2.125.875T11 10q0 1.25.875 2.125T14 13ZM7 8q.825 0 1.412-.588Q9 6.825 9 6H7Zm14 0V6h-2q0 .825.587 1.412Q20.175 8 21 8Z"/></svg>
        EXPENSE<i class="fas fa-angle-right dropdown"></i></a>
            <div class="sub-menu">
                <a href="Expense-expense.php" class="sub-item">Expense</a>
                <a href="Expense-employeesalary.php" class="sub-item">Employee Salary</a>
            </div>
        </div>
        <div id="account" class="item"><a class="sub-btn"> <svg xmlns="http://www.w3.org/2000/svg" height="24" width="24"><path d="M10 12q-1.65 0-2.825-1.175Q6 9.65 6 8q0-1.65 1.175-2.825Q8.35 4 10 4q1.65 0 2.825 1.175Q14 6.35 14 8q0 1.65-1.175 2.825Q11.65 12 10 12Zm-8 8v-2.8q0-.85.425-1.563.425-.712 1.175-1.087 1.5-.75 3.113-1.15Q8.325 13 10 13h.338q.162 0 .312.05-.725 1.725-.588 3.563Q10.2 18.45 11.25 20Zm14 1-.3-1.5q-.3-.125-.563-.262-.262-.138-.537-.338l-1.45.45-1-1.7 1.15-1q-.05-.35-.05-.65 0-.3.05-.65l-1.15-1 1-1.7 1.45.45q.275-.2.537-.338.263-.137.563-.262L16 11h2l.3 1.5q.3.125.563.275.262.15.537.375l1.45-.5 1 1.75-1.15 1q.05.3.05.625t-.05.625l1.15 1-1 1.7-1.45-.45q-.275.2-.537.338-.263.137-.563.262L18 21Zm1-3q.825 0 1.413-.587Q19 16.825 19 16q0-.825-.587-1.413Q17.825 14 17 14q-.825 0-1.412.587Q15 15.175 15 16q0 .825.588 1.413Q16.175 18 17 18Z"/></svg>
        ACCOUNT<i class="fas fa-angle-right dropdown"></i></a>
            <div class="sub-menu">
                <a href="Account-Type.php" class="sub-item" id="account-type">Account Type</a>
                <a href="Account.php" class="sub-item" id="accounts">User Account</a>
            </div>            
        </div>
        <div id="settings" class="item"><a class="sub-btn"><svg xmlns="http://www.w3.org/2000/svg" height="24" width="24"><path d="m9.25 22-.4-3.2q-.325-.125-.612-.3-.288-.175-.563-.375L4.7 19.375l-2.75-4.75 2.575-1.95Q4.5 12.5 4.5 12.337v-.675q0-.162.025-.337L1.95 9.375l2.75-4.75 2.975 1.25q.275-.2.575-.375.3-.175.6-.3l.4-3.2h5.5l.4 3.2q.325.125.613.3.287.175.562.375l2.975-1.25 2.75 4.75-2.575 1.95q.025.175.025.337v.675q0 .163-.05.338l2.575 1.95-2.75 4.75-2.95-1.25q-.275.2-.575.375-.3.175-.6.3l-.4 3.2Zm2.8-6.5q1.45 0 2.475-1.025Q15.55 13.45 15.55 12q0-1.45-1.025-2.475Q13.5 8.5 12.05 8.5q-1.475 0-2.488 1.025Q8.55 10.55 8.55 12q0 1.45 1.012 2.475Q10.575 15.5 12.05 15.5Z"/></svg>
        SETTINGS<i class="fas fa-angle-right dropdown"></i></a>
            <div class="sub-menu">
                <a href="Settings-help.php" class="sub-item" id="settings-help">Help</a>
                <a href="Settings-datalogs.php" class="sub-item" id="settings-datalogs">Data Logs</a>
                <a href="Settings-dataarchive-customers.php" class="sub-item" id="settings-dataarchive">Archive</a>
                <a href="Settings-databackup-customers.php" class="sub-item" id="settings-databackup">Backup/Restore</a>
            </div>
        </div>
    </div>
   </div>
   </div>
            <main>
                <div class="main-pos">
                    <h1 class="posTitle">POINT OF SALES</h1> 
                    <div class="top-buttons">
                        <div class="newUser-button"> 
                            <button type="submit" id="select-customerbutton" class="add-account" onclick="addnewuser();">
                            <svg xmlns="http://www.w3.org/2000/svg" height="20" width="20"><path d="m15.896 17.792-5.125-5.125q-.604.375-1.344.593-.739.219-1.594.219-2.271 0-3.875-1.604T2.354 8q0-2.271 1.604-3.875t3.875-1.604q2.271 0 3.875 1.604T13.312 8q0 .875-.218 1.594-.219.718-.594 1.302l5.146 5.166Zm-8.063-6.771q1.271 0 2.146-.875T10.854 8q0-1.271-.875-2.146t-2.146-.875q-1.271 0-2.145.875-.876.875-.876 2.146t.876 2.146q.874.875 2.145.875Z"/></svg>
                                <h3>SELECT CUSTOMER</h3>
                            </button>
                        </div>
                        <div class="newUser-button"> 
                            <button type="submit" id="new-customerbutton" class="add-account" onclick="addnewuser();">
                                <svg xmlns="http://www.w3.org/2000/svg" height="20" width="20"><path d="M9.25 14h1.5v-3.25H14v-1.5h-3.25V6h-1.5v3.25H6v1.5h3.25Zm.75 4q-1.646 0-3.104-.625-1.458-.625-2.552-1.719t-1.719-2.552Q2 11.646 2 10q0-1.667.625-3.115.625-1.447 1.719-2.541Q5.438 3.25 6.896 2.625T10 2q1.667 0 3.115.625 1.447.625 2.541 1.719 1.094 1.094 1.719 2.541Q18 8.333 18 10q0 1.646-.625 3.104-.625 1.458-1.719 2.552t-2.541 1.719Q11.667 18 10 18Zm0-1.5q2.708 0 4.604-1.896T16.5 10q0-2.708-1.896-4.604T10 3.5q-2.708 0-4.604 1.896T3.5 10q0 2.708 1.896 4.604T10 16.5Zm0-6.5Z"/></svg>
                                <h3>ADD NEW CUSTOMER</h3>
                            </button>
                        </div>
                        <div class="newUser-button"> 
                            <button type="submit" id="return-containerbutton" class="add-account" onclick="addnewuser();">
                            <svg xmlns="http://www.w3.org/2000/svg" height="20" width="20"><path d="M7.479 15.042 2.417 10l5.062-5.042 1.729 1.73-2.083 2.083h7v-2.25h2.458v4.708H7.125l2.083 2.083Z"/></svg>
                                <h3>RETURN CONTAINER</h3>
                            </button>
                        </div>
                    </div>
                    <!-- <div class="top-buttons">
                        <button class="selectCustomer-button">
                            <h3>SELECT CUSTOMER</h3>
                        </button>
                        <button class="newCustomer-button">
                            <h3>NEW CUSTOMER</h3>
                        </button>
                        <button class="returnContainer-button">
                            <h3>RETURN CONTAINER</h3>
                        </button>
                    </div> -->

<!-- ---------------------------------------------------- ORDER DETAILS ------------------------------------------------- -->
        <!-- <div class="form-container"> -->
            <div class="form1">
                <p class="selectCustomer-text">SELECT CUSTOMER</p>
                <div class="delivery-options">
                    <select class="select">
                        <option value="Walk In">Walk In</option>
                        <option value="Delivery">Delivery</option>
                        <option value="Pick Up">Pick Up</option>
                    </select>
                </div>
                <hr>    
                <div class="form1-ordertype-buttons">
                    <a href="Pointofsales.php" class="refillOrder-button">Refill</a>
                    <a href="Pointofsales-new.php" class="newOrder-button">New</a>
                </div>

                <br>

                <div class="form1-details">
                    <div class="form1-tableoption-buttons">
                        <img class="containerlogo" src="../Tag-s-Water-Purified-Drinking-Water/Pictures and Icons/gallon.png">    
                        <button class="containerTable-button">Container</button>
                        <br>
                        <img class="bottlelogo" src="../Tag-s-Water-Purified-Drinking-Water/Pictures and Icons/plastic-bottle.png">    
                        <button class="bottleTable-button">Bottle</button>
                        <br>
                        <img class="menulogo" src="../Tag-s-Water-Purified-Drinking-Water/Pictures and Icons/menu.png">    
                        <button class="othersTable-button">Others</button>
                    </div>
                </div>
                <div class="form1-table">
                    <table class="table" id="myTable"> 
                        <thead> 
                            <tr>
                                <th></th>
                                <th>WATER</th>
                                <th>ITEM</th>
                                <th>Own Gal</th>
                                <th>Tags Gal</th>
                                <th>Price</th>
                                <th>TOTAL</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                            <tr id="selectorder" class="selectorder" style="width:10rem">
                                <td colspan="7"><input type="button" class="select-order" id="selectOrder" value="ADD ORDER"></td>                         
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="form1-buttons">
                    <button class="addDeliveryFee-button">Add Delivery Fee</button>
                    <button class="addOrder-button" id="addOrder-form">Add Order</button>
                </div>
            </div>

     
<!-- CONTAINER END ---------------------------------------------------------------------------------------- -->
<!--------------------------------------------------------------------------------------------------------- -->

          
        </div> 
            </main>
                <div class="top-menu">  
                    <div class="menu-bar">
                        <div class="menu-btn2">
                            <i class="fas fa-bars"></i>
                        </div>
                        <h2 class="posTitle-top">POINT OF SALES</h2>
                        <div class="user1">
                            <div class="welcome">
                                <h4 > Welcome, </h4>
                            </div>
                            <div class="user-name">
                                <h4><?php echo $_SESSION['user_first_name']; ?> </h4>
                            </div>
                            <div class="user-type">
                                <h1><?php echo $_SESSION['user_user_type']; ?> </h1>
                            </div>
                        </div>   
                        <div class="user2"> 
                        <div class="profile" onclick="menuToggle();">
                            <img src="uploaded_image/<?= $_SESSION['user_profile_image']; ?>" alt="">
                        </div>
                        <div class="drop-menu" >
                                <div class="ul">
                                    <div class="user-type3">
                                            <h1><?php echo $_SESSION['user_user_type']; ?> </h1>
                                        </div>
                                    <input type="checkbox" class="checkbox" id="checkbox">
                                        <label for="checkbox" class="theme-dark">
                                        <svg class="moon" xmlns="http://www.w3.org/2000/svg" height="18" width="18"><path d="M10 17q-2.917 0-4.958-2.042Q3 12.917 3 10q0-2.917 2.042-4.958Q7.083 3 10 3q.271 0 .531.021.261.021.531.062-.812.605-1.291 1.5-.479.896-.479 1.917 0 1.771 1.218 2.99 1.219 1.218 2.99 1.218 1.021 0 1.917-.479.895-.479 1.5-1.291.041.27.062.531.021.26.021.531 0 2.917-2.042 4.958Q12.917 17 10 17Z"/></svg>
                                            <svg class="sun" xmlns="http://www.w3.org/2000/svg" height="18" width="18"><path d="M10 14q-1.667 0-2.833-1.167Q6 11.667 6 10q0-1.667 1.167-2.833Q8.333 6 10 6q1.667 0 2.833 1.167Q14 8.333 14 10q0 1.667-1.167 2.833Q11.667 14 10 14Zm-8.25-3.25q-.312 0-.531-.219Q1 10.312 1 10q0-.312.219-.531.219-.219.531-.219h2q.312 0 .531.219.219.219.219.531 0 .312-.219.531-.219.219-.531.219Zm14.5 0q-.312 0-.531-.219-.219-.219-.219-.531 0-.312.219-.531.219-.219.531-.219h2q.312 0 .531.219Q19 9.688 19 10q0 .312-.219.531-.219.219-.531.219ZM10 4.5q-.312 0-.531-.219-.219-.219-.219-.531v-2q0-.312.219-.531Q9.688 1 10 1q.312 0 .531.219.219.219.219.531v2q0 .312-.219.531-.219.219-.531.219ZM10 19q-.312 0-.531-.219-.219-.219-.219-.531v-2q0-.312.219-.531.219-.219.531-.219.312 0 .531.219.219.219.219.531v2q0 .312-.219.531Q10.312 19 10 19ZM5.042 6.104 4 5.042q-.229-.209-.229-.511 0-.302.229-.531.208-.229.521-.229.312 0 .521.229l1.062 1.042q.229.229.229.531 0 .302-.229.531-.208.229-.521.229-.312 0-.541-.229ZM14.958 16l-1.062-1.042q-.229-.229-.229-.531 0-.302.229-.531.208-.229.521-.229.312 0 .541.229L16 14.958q.229.209.229.511 0 .302-.229.531-.229.229-.521.229-.291 0-.521-.229Zm-1.062-9.896q-.229-.208-.229-.521 0-.312.229-.541L14.958 4q.23-.229.521-.219.292.011.521.219.229.229.229.521 0 .291-.229.521l-1.042 1.062q-.229.229-.531.229-.302 0-.531-.229ZM4 16q-.229-.208-.229-.521 0-.312.229-.521l1.042-1.062q.229-.208.531-.208.302 0 .531.208.229.229.219.531-.011.302-.219.531L5.042 16q-.209.229-.511.229-.302 0-.531-.229Z"/></svg>
                                            <div class="ball"></div>
                                        </label>
                                    </input>
                                        <a href="#" class="account">
                                            <svg xmlns="http://www.w3.org/2000/svg" height="20" width="20"><path d="M4.917 14.167q1.062-.875 2.364-1.313 1.302-.437 2.719-.437 1.417 0 2.719.437 1.302.438 2.385 1.313.688-.855 1.084-1.907.395-1.052.395-2.26 0-2.75-1.916-4.667Q12.75 3.417 10 3.417T5.333 5.333Q3.417 7.25 3.417 10q0 1.208.406 2.26.406 1.052 1.094 1.907ZM10 10.854q-1.229 0-2.073-.844-.844-.843-.844-2.072 0-1.23.844-2.073.844-.844 2.073-.844t2.073.844q.844.843.844 2.073 0 1.229-.844 2.072-.844.844-2.073.844Zm0 7.479q-1.729 0-3.25-.656t-2.646-1.781q-1.125-1.125-1.781-2.646-.656-1.521-.656-3.25t.656-3.25q.656-1.521 1.781-2.646T6.75 2.323q1.521-.656 3.25-.656t3.25.656q1.521.656 2.646 1.781t1.781 2.646q.656 1.521.656 3.25t-.656 3.25q-.656 1.521-1.781 2.646t-2.646 1.781q-1.521.656-3.25.656Zm.021-1.75q1.021 0 2-.312.979-.313 1.771-.896-.771-.604-1.75-.906-.98-.302-2.042-.302-1.062 0-2.031.302-.969.302-1.761.906.792.583 1.782.896.989.312 2.031.312ZM10 9.104q.521 0 .844-.323.323-.323.323-.843 0-.521-.323-.844-.323-.323-.844-.323-.521 0-.844.323-.323.323-.323.844 0 .52.323.843.323.323.844.323Zm0-1.166Zm0 7.437Z"/></svg>
                                            <h4>My Account</h4>
                                        </a>
                                        <a href="#" class="help">
                                            <svg xmlns="http://www.w3.org/2000/svg" height="20" width="20"><path d="M10 15q.417 0 .708-.292Q11 14.417 11 14t-.292-.708Q10.417 13 10 13t-.708.292Q9 13.583 9 14t.292.708Q9.583 15 10 15Zm-.75-3.188h1.521q0-.77.135-1.093.136-.323.656-.823.73-.708 1.011-1.208.281-.5.281-1.105 0-1.145-.781-1.864Q11.292 5 10.083 5q-1.062 0-1.843.562-.782.563-1.094 1.521l1.354.563q.188-.584.594-.906.406-.323.948-.323.583 0 .958.333t.375.875q0 .479-.323.854t-.719.729q-.729.667-.906 1.094-.177.427-.177 1.51ZM10 18q-1.646 0-3.104-.625-1.458-.625-2.552-1.719t-1.719-2.552Q2 11.646 2 10q0-1.667.625-3.115.625-1.447 1.719-2.541Q5.438 3.25 6.896 2.625T10 2q1.667 0 3.115.625 1.447.625 2.541 1.719 1.094 1.094 1.719 2.541Q18 8.333 18 10q0 1.646-.625 3.104-.625 1.458-1.719 2.552t-2.541 1.719Q11.667 18 10 18Zm0-1.5q2.708 0 4.604-1.896T16.5 10q0-2.708-1.896-4.604T10 3.5q-2.708 0-4.604 1.896T3.5 10q0 2.708 1.896 4.604T10 16.5Zm0-6.5Z"/></svg>
                                            <h4>Help</h4>
                                        </a>
                                        <a href="logout.php" class="logout">
                                            <svg xmlns="http://www.w3.org/2000/svg" height="20" width="20"><path d="M4.5 17q-.625 0-1.062-.438Q3 16.125 3 15.5v-11q0-.625.438-1.062Q3.875 3 4.5 3H10v1.5H4.5v11H10V17Zm9-3.5-1.062-1.062 1.687-1.688H8v-1.5h6.125l-1.687-1.688L13.5 6.5 17 10Z"/></svg>
                                            <h4>Logout</h4>
                                        </a>
                                </div>    
                        </div>  
                    </div>      
                    <!-- <div class="form2"> -->
                            <div class="payment-options">
                                <?php
                                    $dropdown_query1 = "SELECT * FROM payment_option";
                                    $result3 = mysqli_query($con, $dropdown_query1);
                                ?>
                                <p class="paymentOptions-text">Payment Options</p>
                                    <select class="paymentOptions-dropdown">
                                        <?php while($row3 = mysqli_fetch_array($result3)):;?>
                                            <option><?php echo $row3[1];?></option>
                                        <?php endwhile;?>
                                    </select>
                            </div>
                            <div class="totalOrder">
                                
                                <header class="company-name">Tag's Water Purified Drinking Water</header>
                                <body>
                                    <p class="date-Text">Date and Time: 
                                        <div class="card">
                                            <h1 id="time" class="time">00:00:00</h1>
                                            <h1 class="dash">-</h1>
                                            <h1 id="date" class="date">00/00/0000</h1>
                                        </div>
                                    </p>
                                    <hr class="hr1">
                                    <p class="orderSummary-text">Order Summary</p>
                                    <p class="cashier-text">Cashier: <span id="cashier-name"><h5 class="name-cashier"><?php echo $_SESSION['user_first_name']; ?></h5></span></p>
                                    <div class="orderSum-table">
                                        <table class="tableCheckout" id="myTable"> 
                                            <thead> 
                                                <tr>
                                                    <th>ITEM</th>
                                                    <th>Water</th>
                                                    <th>Type</th>
                                                    <th>QTY</th>
                                                    <th>Price</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                    <hr>
                                    <p class="orderTotal-text">Order Total</p>
                                    <h3 class="peso-sign">PHP</h3>
                                    <input type="text" class="total-order" value="0.00" readonly>
                                    <hr>
                                    <div class="receipt-buttons">
                                        <a href="Pointofsales.php" id="cancel">CANCEL</a>   
                                        <input type="button" class="confirmOrder-button" value="CONFIRM">
                                    </div>
                                </body>
                            </div>
                    <!-- </div>  -->
                </div>
                   <!-- ---------------------------------------------------- PREVIOUS TRANSACTIONS ------------------------------------------------- -->
            <div class="form3">
                <div class="previous-transaction">
                    <br>
                    <header class="previous-transaction-header">Previous Transaction</header>
                    <hr>
                    <table class="previous-transaction-table">
                        <thead>
                            <tr>
                            <th>ID</th>
                            <th>Customer Name</th>
                            <th>Item Name</th>
                            <th>Water</th>
                            <th>Type</th>
                            <th>Price</th>
                            <th>QTY</th>
                            <th>Total</th>
                            <th>Change</th>
                            <th>Amount Tentered</th>
                            <th>Payment</th>
                            <th>Service</th>
                            <th>Cashier Name</th>
                            <th>Date/Time</th>
                            </tr>
                        </thead>
                        <?php
                                $dropdown_query2 = "SELECT * FROM transaction";
                                $result4 = mysqli_query($con, $dropdown_query2);
                                    while ($rows = mysqli_fetch_assoc($result4))
                                    {
                                ?>
                                <tbody>
                                        <tr>
                                            <td> <?php echo $rows['transaction_id']; ?></td>
                                            <td> <?php echo $rows['customer_name']; ?></td>
                                            <td> <?php echo $rows['item_name']; ?></td>
                                            <td> <?php echo $rows['water_type']; ?></td>
                                            <td> <?php echo $rows['water_service']; ?></td>
                                            <td> <?php echo $rows['price']; ?></td>
                                            <td> <?php echo $rows['quantity']; ?></td>
                                            <td> <?php echo $rows['total_amount']; ?></td>
                                            <td> <?php echo $rows['customer_change']; ?></td>
                                            <td> <?php echo $rows['amount_tentered']; ?></td>
                                            <td> <?php echo $rows['payment_option']; ?></td>
                                            <td> <?php echo $rows['service_type']; ?></td>
                                            <td> <?php echo $rows['cashier_name']; ?></td>
                                            <td> <?php echo $rows['date_time']; ?></td>
                                        <tr id="noRecordTR" style="display:none">
                                            <td colspan="10">No Record Found</td>                         
                                        </tr>
                                </tbody>
                                        <?php
                                    }
                                    ?> 
                        </table>
                    </div>
                </div>
        </div>
                
<!-- CONTAINER START -------------------------------------------------------------------------------------- -->
<!--------------------------------------------------------------------------------------------------------- -->

                    <?php
                        $dropdown_query = "SELECT item_name FROM inventory_item";
                        $result1 = mysqli_query($con, $dropdown_query);
                    ?>
                    
    </body>
</html>
<script>
       // -----------------------------add order table
        $(document).ready(function(){
            $("#selectOrder").click(function(){
                var addcontrols="<tr>"
                    addcontrols+="<td><button type='button' class='removeBtn'>X</button></td>"
                    addcontrols+="<td><select class='selectTable-water1'><option value='Alkaline'>Alkaline</option><option value='Mineral'>Mineral</option></select></td>"
                    addcontrols+="<td><select class='selectTable-item'><?php while($row2 = mysqli_fetch_array($result1)):;?><option><?php echo $row2[1];?></option><?php endwhile;?></select></td>"
                    addcontrols+="<td><input type='number' class='textBox-table' min='0' placeholder='0' onkeypress='return isNumberKey(event)'></td>"
                    addcontrols+="<td><input type='number' class='textBox-table' min='0' placeholder='0' onkeypress='return isNumberKey(event)'></td>"
                    addcontrols+="<td><input type='text' class='textBox-table' min='0' placeholder='0' onkeypress='return isNumberKey(event)' readonly></td>"
                    addcontrols+="<td><input type='text' class='textBox-table' min='0' placeholder='0' onkeypress='return isNumberKey(event)' readonly></td>"
                    addcontrols+="</tr>";
                        $("table tbody").append(addcontrols);
                });
            });
            $('.table tbody').on('click','.removeBtn',function(){
                $(this).closest('tr').remove();
            });
       // -----------------------------date and time
        var today = new Date();
        var day = today.getDate();
        var month = today.getMonth() + 1;

        function appendZero(value) {
            return "0" + value;
        }

        function theTime() {
            var d = new Date();
            document.getElementById("time").innerHTML = d.toLocaleTimeString("en-US");
        }

        if (day < 10) {
            day = appendZero(day);
        }

        if (month < 10) {
            month = appendZero(month);
        }

        today = day + "/" + month + "/" + today.getFullYear();

        document.getElementById("date").innerHTML = today;

        var myVar = setInterval(function () {
            theTime();
        }, 1000);
     // -----------------------------SIDE MENU
        $(document).ready(function(){
        //jquery for toggle sub menus
        $('.sub-btn').click(function(){
        $(this).next('.sub-menu').slideToggle();
        $(this).find('.dropdown').toggleClass('rotate');
        });

        //jquery for expand and collapse the sidebar
        $('.menu-btn').click(function(){
        $('.side-bar').addClass('active');
        $('.menu-btn').css("visibility", "hidden");
        });

        $('.close-btn').click(function(){
        $('.side-bar').removeClass('active');
        $('.menu-btn').css("visibility", "visible");
        });
        $('.menu-btn2').click(function(){
        $('.side-bar').addClass('active');
        $('.menu-btn2').css("visibility", "hidden");
        });

        $('.close-btn').click(function(){
        $('.side-bar').removeClass('active');
        $('.menu-btn2').css("visibility", "visible");
        });
    });
//    --------------------------------------------------------------------
    const sideMenu = document.querySelector('#aside');
    const closeBtn = document.querySelector('#close-btn');
    const menuBtn = document.querySelector('#menu-button');
    const checkbox = document.getElementById('checkbox');
        menuBtn.addEventListener('click', () =>{
            sideMenu.style.display = 'block';
        })

        closeBtn.addEventListener('click', () =>{
            sideMenu.style.display = 'none';
        })
         checkbox.addEventListener( 'change', () =>{
             document.body.classList.toggle('dark-theme');
        //     if(this.checked) {
        //         body.classList.add('dark')
        //     } else {
        //         body.classList.remove('dark')     
        //     }
         });
    function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
    }
</script>
<style>
    :root{
        --color-main: rgb(2, 80, 2);
        --color-white: white;
        --color-white-secondary: white;
        --color-tertiary: hsl(0, 0%, 57%);
        --color-black: rgb(49, 49, 49);
        --color-maroon: rgb(136, 0, 0);
        --color-secondary-main: rgb(244, 255, 246);
        --color-background: rgb(235, 235, 235);
        --color-solid-gray: rgb(126, 126, 126);
        --color-td:rgb(100, 100, 100);
        --color-blue-button: rgb(62, 178, 255);
        --color-button: rgb(117, 117, 117);
        --color-table-shadow: rgb(244, 255, 246);
        --color-shadow-shadow: rgb(116, 116, 116);
        --color-table-hover: rgb(244, 255, 246);
        --color-aside-mobile-focus: rgb(78, 150, 78);
        --color-aside-mobile-text: hsl(0, 0%, 57%);
        --color-select-customer:rgb(9, 138, 107);
        --color-new-customer:rgb(169, 109,5);
        --color-return-container:rgb(54, 85, 225);
        --color-table-title:rgb(0, 197, 145);
        --color-table-border:rgb(226, 226, 229);
        --color-secondary-background:rgb(244, 244, 244);
        --color-lightest-gray:rgb(250,250,250);
    }
    .dark-theme{
        --color-white: rgb(48, 48, 48);
        --color-tertiary: hsl(0, 0%, 25%);
        --color-black: white;
        --color-shadow-shadow: rgb(32, 32, 32);
        --color-aside-mobile-focus: rgb(244, 255, 246);
        --color-table-shadow: rgb(131, 131, 131);
        --color-maroon: rgb(255, 130, 130);
        --color-blue-button: rgb(164, 219, 255);
        --color-white-secondary: rgb(235, 235, 235);
        --color-main: rgb(244, 255, 246);
        --color-secondary-main: rgb(97, 172, 111);
        --color-background: rgb(80, 80, 80);
        --color-solid-gray: rgb(231, 231, 231);
        --color-td: rgb(231, 231, 231);
        --color-button: rgb(202, 202, 202);
        --color-table-hover: rgb(112, 112, 112);
        --color-aside-mobile-text:hsl(0, 0%, 88%);
    }
    BODY{
        background: var(--color-background);
        margin: 0;
        padding: 0;
        height: 100%;
        overflow-x: hidden;
        font-family: Arial, Helvetica, sans-serif;
        background-position: center;
        background-size: cover;
        background-attachment: fixed;
    }  
  
      /* -----------------------------------------------Side Menu---------------------------------------- */
      .side-bar{
        background: var(--color-table-hover);
        backdrop-filter: blur(15px);
        width: 15.5rem;
        height: 100vh;
        position: fixed;
        top: 0;
        /* left: -100%; */
        overflow-y: auto;
        transition: 0.6s ease;
        transition-property: left;
    }
    .side-bar .title{
            display: flex;
            align-items: center;
            justify-content: space-between;
            margin-top: -1.9rem;
        }
        .side-bar .titlelogo{
            display: flex;
            gap: 0.8rem;
        }
        .side-bar .titlelogo img{
            width: 5rem;
            margin-top: -1rem;
            margin-bottom: 1rem;
            margin-left: 5.5rem;
        }
        .side-bar .close{
            display: none;
            font-size: 1rem;
        }
    .side-bar.active{
        left: 0;
    }

    .side-bar .menu{
        width: 100%;
        margin-top: 80px;
    }

    .side-bar .menu .item{
        position: relative;
        cursor: pointer;
    }

    .side-bar .menu .item a{
        color: var(--color-tertiary);
        font-size: 13px;
        text-decoration: none;
        display: flex;
        fill: var(--color-tertiary);
        margin-left: .5rem;
        gap: 1rem;
        align-items: center;
        position: relative;
        padding: 0px 20px;
        line-height: 60px;
        height: 3.7rem;
        transition: all 300ms ease;
    }
    .side-bar .menu .item .sub-item{
        height: 2.9rem;

    }
    #pointofsales{
            background: var(--color-white);
            transition: 0.6s;
            color: var(--color-main);
            fill: var(--color-main);
            margin-left: 0;
            padding-left: 1rem;
            content: "";
            margin-bottom: 6px;
            font-size: 15px;
            border-radius: 0 0 10px 0 ;
            box-shadow: 1px 3px 1px var(--color-background);
        }
    .side-bar .menu .item a:hover{
        background: var(--color-table-hover);
        transition: 0.6s;
        margin-left: 0rem;
        border-radius: 0 10px 10px 0 ;
        box-shadow: 2px 2px 2px rgb(224, 224, 224);
    }

    .side-bar .menu .item a .dropdown{
        position: absolute;
        right: 0;
        margin: 20px;
        transition: 0.3s ease;
    }

    .side-bar .menu .item .sub-menu{
        background: var(--color-background);
        display: none;
        
    }

    .side-bar .menu .item .sub-menu a{
        padding-left: 90px;
        font-size: 13px;
        font-weight: 500;
        font-family: 'switzer', sans-serif;
        box-shadow: 0px 1px 1px rgb(224, 224, 224);

    }

    .rotate{
        transform: rotate(90deg);
    }

    .close-btn{
        position: absolute;
        color: var(--color-tertiary);
        font-size: 14px;
        right: 0;
        margin: 25px;
        margin-top: 50px;
        display: none;
        cursor: pointer;
    }

    .menu-btn{
        position: absolute;
        color: var(--color-tertiary);
        font-size: 20px;
        margin: 25px;
        cursor: pointer;
    }
    .menu-btn:hover{
        color: var(--color-main);
    }
    .menu-btn2{
        position: absolute;
        color: var(--color-tertiary);
        font-size: 20px;
        margin: 25px;
        display: none;
        left: 0;
        cursor: pointer;
    }
    .menu-btn2:hover{
        /* position: absolute; */
        color: var(--color-main);
        /* font-size: 25px;
        margin: 30px;
        cursor: pointer; */
    }
     /* ----------------------------------------Top bar menu----------------------------------------  */
     .top-menu{
        margin-top: .7rem;
        position: absolute;
        display: grid;
        right: 3%;
        width:100%;
    }
    .top-menu .menu-bar{
        display: flex;
        justify-content: end;
        gap: 2rem;
        position:relative;
    }
    .top-menu .menu-bar button{
        display: none;
    }
    .top-menu .menu-bar .user1{
        gap: 2rem;
        align-items: right;
        text-align: right;
    }
    .top-menu .menu-bar .user2{
        display: flex;
        gap: 2rem;
        align-items: right;
        text-align: right;
    }
    .top-menu .menu-bar .posTitle-top{
        font-size: min(max(1.2rem, 0.4vw), 1.3rem);
        color: var(--color-main); 
        font-family: 'COCOGOOSE', sans-serif;
        letter-spacing: .03rem;
        display: none;
        text-align: center;
        align-items: center;
    }
    
    .user-type{
        font-family: 'switzer', sans-serif;
        font-size: 7.5px;
        color: var(--color-black); 
        letter-spacing: 1px;
        border-top: 2px solid var(--color-main); 
        margin-top: -0.97rem;
        width: 7vw;
        text-transform: uppercase;
    }
    h1{
        margin-top: 6px;     
    }
    .welcome{
        font-family: 'Calibri', sans-serif;
        font-size: 11px;
        /* margin-right: -7.3rem;*/
        margin-top: -0.6rem; 
        letter-spacing: 2px;
        color: var(--color-main); 
    }
    .user-name{
        font-family: 'Switzer', sans-serif;
        font-size: 12px;
        margin-top: -1rem; 
        text-transform: uppercase;
        margin-bottom: 0;
        color: var(--color-maroon);
    }
    .profile img{
        background: var(--color-white); 
        border-radius: 30%;
        width: 50px;
        padding: 4px;
    }
    #menu-button{
        border: none;
        background: none;
    }
    a{
        text-decoration:none;
        font-family: 'COCOGOOSE', sans-serif;
    }
    .user2 a{
        font-family: 'Malberg Trial', sans-serif;
        color: rgb(68, 68, 68);
    }
    .todeliver{
        margin-bottom: -2.2rem;
        margin-top: 2rem;
        color: rgb(117, 117, 117);
        font-size: 1.3rem;
        letter-spacing: .1rem;
        font-family: 'Galhau Display', sans-serif;
    }
    h3{
        font-size: 0.87rem;
    }
    .user2 .profile{
        position: relative;
        cursor: pointer;
    }
    .user2 .drop-menu{
        position: absolute; 
        top: 120px;
        right: 15px;
        padding: 10px 20px;
        background: var(--color-white);
        width: 110px;
        box-sizing: 0 5px 25px rgba(0,0,0,0.1);
        border-radius: 7px;
        transition: 0.5s;
        visibility: hidden;
        opacity: 0;
    }
    .user2 .drop-menu.user2{
        top: 80px;
        visibility: visible;
        opacity: 1;
    }
    .user2 .drop-menu::before{
        content:'';
        position: absolute;
        top: -5px;
        right: 33px;
        width: 15px;
        height: 20px;
        background: var(--color-white);
        transform: rotate(45deg);
        transition: 0.5s;
    }
    .drop-menu .ul .user-type3{
        font-family: 'PHANTOM', sans-serif;
        font-size: 7.5px;
        color: var(--color-main); 
        letter-spacing: .2rem;
        display: none;
    }

    .drop-menu .ul{
        margin-top: 2rem;
        display: flex;
        flex-direction: column;
        height: 9vh;
        position: relative;
        margin-bottom: 0.5rem;
    }
    .drop-menu h4{
        font-weight: 400;
        font-size: 12px;
    }
    .drop-menu .ul a{
        display: flex;
        color: hsl(0, 0%, 69%);
        fill: hsl(0, 0%, 69%); 
        margin-left: -1.26rem;
        padding-left: 1rem;
        gap: 1rem;
        height: 1rem;
        width: 8.5rem;
        align-items: center;
        position: relative;
        height: 1.7rem;
        transition: all 300ms ease;
    }
    .drop-menu .ul a:hover {
        background:  rgb(190, 190, 190);
        transition: 0.6s;
        color: var(--color-white);
        fill: var(--color-white);
        padding-left: .9rem;
        content: "";
        margin-bottom: 6px;
        font-size: 15px;
        border-radius: 0px 0px 10px 10px;
        cursor: pointer;
    }
    .checkbox{
        opacity: 0;
        position: absolute;
    }
    .checkbox:checked + .theme-dark .ball{
        transform: translateX(28px);
    }
    .drop-menu .theme-dark{
        background: hsl(0, 0%, 69%);
        display: flex;
        justify-content: space-between;
        align-items: center;
        height: 14.5px;
        width: 42.5px;
        cursor: pointer;
        border-radius: 50px;
        position: relative;
        padding: 5px;
        margin-top: -30px;
        margin-bottom: 8px;
        margin-left: 2rem;
    }
    .sun{
        fill: yellow;
    }
    .moon{
        fill: white;
    }
    .ball{
        background: white;
        position: absolute;
        border-radius: 50%;
        top: 2px;
        left: 2px;
        height: 21px;
        width: 21px;
        transition: transform 0.2s linear;
    }
    /* ----------------------------------------MAIN---------------------------------------- */
    .main-pos{
        width:100%;
    }
    .posTitle{
        margin-top: 2rem;
        font-size: min(max(1.9rem, 1.1vw), 2rem);
        color: var(--color-main); 
        font-family: 'COCOGOOSE', sans-serif;
        letter-spacing: .03rem;
        border-bottom: 2px solid var(--color-main); 
        width: 100%;
    }
   
        /* ----------------------------------------Sub TAB---------------------------------------- */
        /* .user-title{
            position: relative;
        }
        main  h2{
            margin-bottom: -2.2rem;
            margin-top:2rem;
            color: var(--color-solid-gray);
            font-size: 1.3rem;
            margin-left: 3%;
            letter-spacing: .1rem;
            font-family: 'Galhau Display', sans-serif;
        }
        main .sub-tab{
            margin-bottom: 4rem;
        } */
        /* ----------------------------------------Search BAR---------------------------------------- */
        /* .search{
            position: absolute;
            gap: 2rem;
            align-items: right;
            text-align: right;
            left: 50%;
        }
        .search-bar{
            width: 18rem;
            background: var(--color-white);
            display: flex;
            position: relative;
            align-items: center;
            border-radius: 60px;
            padding: 10px 20px;
            height: 1.8rem;
            backdrop-filter: blur(4px) saturate(180%);
        }
        .search-bar input{
            background: transparent;
            flex: 1;
            border: 0;
            outline: none;
            padding: 24px 20px;
            font-size: .8rem;
            color: var(--color-black); 
            margin-left: -0.95rem;
        }
        ::placeholder{
            color: var(--color-solid-gray);
            
        }
        .search-bar button svg{
            width: 20px;
            fill: var(--color-white); 
        }
        .search-bar button{
            border: 0;
            border-radius: 50%;
            width: 35px;
            height: 35px;
            background: var(--color-main); 
            margin-right: -0.55rem;
        } */
        /* ----------------------------------------Add Button---------------------------------------- */
        .newUser-button{
            position: relative;
            margin-top: .5rem;
            display: inline-block;
            margin-left: 1rem;
        }
        #select-customerbutton:hover{
            background: var(--color-select-customer);
            color: var(--color-white); 
            fill: var(--color-white); 
        }
        #return-containerbutton:hover{
            background: var(--color-return-container);
            color: var(--color-white); 
            fill: var(--color-white); 
        }
        #new-customerbutton:hover{
            background: var(--color-new-customer);
            color: var(--color-white); 
            fill: var(--color-white); 
        }
        .add-account{
            display: flex;
            border: none;
            background-color: var(--color-white); 
            align-items: center;
            color: var(--color-button); 
            fill: var(--color-button); 
            width: 13rem;
            /* max-height: 46px; */
            border-radius: 20px;
            /* padding: .68rem 1rem; */
            font-family: 'Outfit', sans-serif;
            cursor: pointer; 
            gap: 1rem;
            height: 3.7rem;
            transition: all 300ms ease;
            margin-top: .2rem;
            text-transform: uppercase;
            padding: 1rem;
            margin-left: 1rem;
            border-radius: 5rem;
            font-size: 1rem;
            height: 3rem;
            box-shadow: 0px 2px 0px 0px var(--color-shadow-shadow);
        }
        .add-account h3{
            font-size: .8rem;
        }
        .add-account:hover{
            background-color: var(--color-table-hover); 
            color: var(--color-main);
            fill: var(--color-main);
            padding-top: -.2px;
            transition: 0.3s;
            border-bottom: 4px solid var(--color-maroon);
        }
         /* ----------------------------------------Dashboard Table---------------------------------------- */
     /* main .account-container{
        margin-top: 2rem;
        height: 500px;
        
    }*/
     table{
        background: var(--color-white);
        font-family: 'Switzer', sans-serif;
        width: 100%;
        font-size: 0.8rem;
        border-radius: 0px 0px 10px 10px;
        padding-left: 0.5rem;
        padding-right: 0.5rem;
        padding-bottom: 2.5rem;
        text-align: center; 
        transition: all 700ms ease;
        overflow: auto;
        margin-top: -1rem;
    }

    table tbody td{
        height: 2.8rem;
        border-bottom: 1px solid var(--color-solid-gray);
        color: var(--color-td); 
        font-size: .67rem;
    }
     th{
        height: 1.8rem;
        color: var(--color-black); 
        margin:1rem;
        font-size: .8rem;
        letter-spacing: 0.02rem;
        border-bottom: 2px solid var(--color-solid-gray);
    }  
    tr:hover td{
        color: var(--color-main); 
        cursor: pointer;
        background-color: var(--color-table-hover);
     } 
    /* ----------------------------------------ASIDE---------------------------------------- */
    .container{
        display: grid;
        width: 71%;
        /* margin: 0 auto; */
        background: var(--color-background);
        gap: 1.8rem;
        grid-template-columns: 16rem auto;
    }
    #menu-button{
        display: none;
    }
      @media screen and (max-width: 1600px){
        .container{
            width: 94%;
            grid-template-columns: 16rem auto;
        }
   
        #aside .titlelogo2 img{
            margin-left: 1.8rem;
            width: 40%;
        }

        #aside .sidebar2 a{
            width: 5.95rem;
        }
        #aside .sidebar2 a:focus{
            padding-left: 2rem;
            width: 4rem;
        }
        .top-menu{
            width: 370px;
        }
        .main-dashboard{
            position: relative;
            left: -5%;
        }
        main .account-container{
            margin: 2rem 0 0 8.8rem;
            width: 94%;
            position: absolute;
            left: 0;
            margin-left: 52%;
            transform: translateX(-50%);
            margin-top: 3%;
        }
        main .account-container table{
            width: 65vw;
            padding-left:30px;
            padding-right:30px;
        }
        .dashTitle{
            margin-left: 5%;
            width: 78%;
            margin-top: 3.2rem;
        }
        main  h2{
            margin-left: 10%;
        }
        main .sub-tab{
            margin-bottom: 4rem;
        }
        .newUser-button{
            margin-left: 1rem;
        }
        .search{
            left: 55%;
        }
        .search-bar{
            width: 17vw;
        }
    }
    @media screen and (max-width: 1400px){
        .container{
            width: 94%;
            grid-template-columns: 4rem auto;
        }
        .side-bar{
            z-index: 3;
            position: fixed;
            left: -100%;
        }
        .close-btn{
            display: flex;
        }

        .top-menu{
            width: 370px;
        }
        .main-dashboard{
            position: relative;
            left: -5%;
        }
        main .account-container{
            margin: 2rem 0 0 8.8rem;
            width: 94%;
            position: absolute;
            left: 0;
            margin-left: 52%;
            transform: translateX(-50%);
            margin-top: 3%;
        }
        main .account-container table{
            width: 65vw;
            padding-left:30px;
            padding-right:30px;
        }
        .dashTitle{
            margin-left: 5%;
            /* margin-top: 3.5rem; */
            width: 60vw;

        }
        main  h2{
            margin-left: 10%;
        }
        main .sub-tab{
            margin-bottom: 4rem;
        }

        .search{
            left: 55%;
        }
        .search-bar{
            width: 17vw;
        }
    }
    @media screen and (max-width: 1200px){
        .container{
            width: 94%;
            grid-template-columns: 4rem auto;
        }
    
        .top-menu{
            width: 370px;
        }
        .main-dashboard{
            position: relative;
            left: -5%;
        }
        main .account-container{
            margin: 2rem 0 0 8.8rem;
            width: 94%;
            position: absolute;
            left: 0;
            margin-left: 50%;
            transform: translateX(-50%);
            margin-top: 3%;
        }
        main .account-container table{
            width: 80vw;
            padding-left:30px;
            padding-right:30px;
        }
        .dashTitle{
            margin-left: 5%;
            width: 60vw;
        }
        main  h2{
            margin-left: 10%;
        }
        main .sub-tab{
            margin-bottom: 4rem;
        }
        .newUser-button{
            left: 137%;
        }
        .search{
            left: 77%;
        }
        .search-bar{
            width: 20vw;
        }
        .user2 .drop-menu{
            right: 13px;
            margin-top: 2px;
        }
        .user2 .drop-menu::before{
            right: 25px;
        }
        .drop-menu .ul{
            width: 8.5rem;
            height: 5rem;
        }
        .drop-menu .ul a{
            width: 8.5rem;
        }
    }

    @media screen and (max-width: 768px){
        .containter{
            width: 100%;
        }
    
        .menu-btn2{
            display: flex;
        }
        .top-menu{
            width: 94%;
            margin: 0 auto 4rem;
        }
        .top-menu .menu-bar{
            position: fixed;
            top: 0;
            left: 0;
            align-items: center;
            padding: 0 0.8rem;
            height: 4rem;
            background: var(--color-white);
            width: 100%;
            margin: 0;
            z-index: 2;
            box-shadow: 0px 1px 14px var(--color-shadow-shadow);
        }
        .top-menu .menu-bar .dashTitle-top{
            display: block;
            left: 0;
            margin-left: 4rem;
            position: absolute;
        }
         .profile{
            margin-right: 1.4rem;
        }
        .top-menu .menu-bar .user1{
            display: none;
        }
        .drop-menu .ul .user-type3{
            display: block;
            left:22.5%;
            position: absolute; 
            margin-top: -2.3rem;
            margin-bottom: 1.9rem;
        }
        .dashTitle{
            display:none;
        }
        .user2 .drop-menu{
            right: 40px;
            height: 9.3rem;
            margin-top: 2px;
        }
        .user2 .drop-menu::before{
            right: 17px;
        }
        .drop-menu .ul{
            width: 8.5rem;
            height: 5rem;
        }
        .drop-menu .ul .theme-dark{
            margin-top: -.3rem;
        }
        
        .drop-menu .ul a{
            width: 8.5rem;
        }
        .main-dashboard{
            position: relative;
            left: -5%;
        }
        main .account-container{
            margin: 2rem 0 0 8.8rem;
            width: 94%;
            position: absolute;
            display:none;
            left: 0;
            margin-left: 50%;
            transform: translateX(-50%);
            margin-top: 3%;
        }
        main .account-container table{
            width: 80vw;
            padding-left:30px;
            padding-right:30px;
        }
        main  h2{
            margin-left: 10%;
            display:none;
        }
        main .sub-tab{
            margin-bottom: 4rem;
        }
        .newUser-button{
            left: 137%;
            display:none;
        }
        .search{
            left: 77%;
            display:none;
        }
        .search-bar{
            width: 20vw;
        }
    }
    

    .menu-tab p{
        font-size: 20px;
        font-weight: lighter;
        margin-left: 10px;
    }

    .menu-tab img{
        width: 15px;
        margin-right: 10px;
        margin-left: 20px;
    }
    .menu-tab a:hover{
        background:  rgb(250, 255, 251);
        transition: 0.6s;
        margin-left: 0rem;
        color: rgb(187, 187, 187);
        fill: rgb(187, 187, 187);
        font-weight: bold;
        padding-left: 1rem;
        content: "";
        margin-bottom: 6px;
        font-size: 9px;
        border-radius: 0 10px 10px 0 ;
        box-shadow: 1px 1px 1px rgb(224, 224, 224);
    }

/*------------------------------------------------CONTAINER----------------------------------------------------*/

    /* GENERAL -----------------------------------------------------------------------------------------*/
    button:active {
        background-color: var(--color-blue-button);
    }
    button:hover {
        background-color: var(--color-blue-button);
        color: var(--color-white);
        transition: 0.3s;
    }
    /* button:hover{
        filter: brightness(100%);
        transition: 0.3s;
        /* font-weight: bolder; */
        /* font-size: 110%; */
    /* } */ 
    hr{
        width: 95%;
    }

    /* TOP BUTTONS -------------------------------------------------------------------------------------*/
    .top-buttons{
        display: inline-block;
        /* margin-top: 1rem;
        margin-left: 1rem; */
        /* padding: 50px; */
        /* background-color: none; */
        position: relative;
        margin-bottom: -1rem;
    }
   
    .selectCustomer-button{
        background: var(--color-select-customer);
        padding: 1rem;
        /* padding: .68rem 1rem; */
        margin-left: 1rem;
        border-radius: 5rem;
        font-size: 1rem;
        color: white;
        border: none;
        height: 3rem;
        width: 15rem;
        font-family: 'Outfit', sans-serif;
        box-shadow: 0px 3px 0px 0px var(--color-shadow-shadow);
        margin-top: .2rem;
    }
    .newCustomer-button{
        background: var(--color-new-customer);
        padding: 1rem;
        /* padding: .68rem 1rem; */
        margin-left: 1rem;
        border-radius: 5rem;
        font-size: 1rem;
        color: white;
        border: none;
        height: 3rem;
        width: 15rem;
        font-family: 'Outfit', sans-serif;
        box-shadow: 0px 3px 0px 0px var(--color-shadow-shadow);
        margin-top: .2rem;
    }
    .returnContainer-button{
        background: var(--color-return-container);
        /* padding: 1rem; */
        padding: .68rem 1rem;
        margin-left: 1rem;
        border-radius: 5rem;
        font-size: 1rem;
        color: white;
        border: none;
        height: 3rem;
        font-family: 'Outfit', sans-serif;
        width: 15rem;
        box-shadow: 0px 3px 0px 0px var(--color-shadow-shadow);
        margin-top: .2rem;
    }

    /* ORDER FORM -------------------------------------------------------------------------------*/
    .form-container{
        display: inline-block;
        position: relative;
    }
    .form1{
        background-color: var(--color-white);
        margin-top: 2rem;
        /* margin-left: 20rem; */
        border: none;
        /* border-color: var(--color-table-border); */
        width: 100%;
        display: inline-block;
        height: 33%;
        border-radius: 1rem;
        /* position: absolute; */
        padding-top: 1rem;
    }
    
    .delivery-options{
        display: inline-block;
        /* display: inline-block; */
        position: relative;
        /* margin-left: 18rem; */
        /* text-align: right;    */
        left: 25vw; 
        margin-top: -.7rem;
    }
    .select{
        background: var(--color-solid-gray);
        color: var(--color-white);
        /* align-items: center; */
        border-radius: 15px;
        padding: 10px 13px;
        height: 2.9em;
        width: 10.8vw;
        font-size: 14px;
        cursor: pointer;
        transition: 0.3s;
        border: none;
    }
    .select:hover{
        background: var(--color-main);
    }
    .selectTable-water{
        background: none;
        color: var(--color-tertiary);
        /* align-items: center; */
        /* padding: 10px 13px; */
        height: 2.9em;
        width: 4vw;
        font-size: 14px;
        cursor: pointer;
        border: none;
    }
    .selectCustomer-text{
        display: inline-block;
        color: var(--color-solid-gray);
        font-weight: bold;
        /* margin-top: 2rem; */
        margin-left: 5rem;
        font-size: 25px;
    }
    .selectCustomer-text:hover{
        filter: brightness(50%);
        cursor: pointer;
    }
    .form1-ordertype-buttons{
        margin-top: 1rem;
        margin-left: 20vw;
        width: 100%;
        /* display: block; */
        height: 2rem;
        position: static;
        align-items: right;
        right: 0;
        /* text-align: right; */
    }
    .refillOrder-button{
        background-color: var(--color-secondary-main);
        color: var(--color-tertiary);
        display: inline-block;
        /* padding: 0rem; */
        height: 2rem;
        width: 18rem;
        font-family: 'calibri', sans-serif;
        text-transform: uppercase;
        font-size: 1rem;
        font-weight: 900;
        padding-top: .7rem;
        border-radius: 10px;
        border: none;
        align-items: center;
        text-align: center;
        border-color: var(--color-lightest-gray);
        box-shadow: 1px 0px 3px 1px var(--color-tertiary);
    }
    .a .refillOrder-button{
        text-align: center;
        align-items: center;
    }
    .refillOrder-button:hover{
        background-color: var(--color-blue-button);
        color: var(--color-white);
        transition: 0.3s;
    }
    .refillOrder-button:focus{
        color: var(--color-white);
        transition: 0.3s;
        background-color: var(--color-blue-button);
    }

    .newOrder-button{
        color: var(--color-white);
        background-color: var(--color-blue-button);
        display: inline-block;
        padding: 0rem;
        height: 2rem;
        width: 18rem;
        font-family: 'calibri', sans-serif;
        text-transform: uppercase;
        font-size: 1rem;
        font-weight: 900;
        padding-top: .7rem;
        border-radius: 10px;
        border: none;
        margin-left: 1rem;
        border-color: var(--color-lightest-gray);
        box-shadow: 1px 0px 3px 1px var(--color-tertiary);
        text-align: center;
        align-items: center;
    }
    .newOrder-button:hover{
        color: var(--color-white);
        transition: 0.3s;
        background-color: var(--color-blue-button);
    }
    .newOrder-button:focus{
        color: var(--color-white);
        transition: 0.3s;
        background-color: var(--color-blue-button);
    }
    .form1-details{
        /* width: 50rem; */
        margin-top: 0.5rem;
        margin-left: 1rem;
        display: inline-block;
    }
    .form1-tableoption-buttons{
        background-color: none;
        margin-left: 1rem;
        float: left;
        font-family: 'outfit', sans-serif;
        display: inline-block;
        border: none;
    }
    .containerlogo{
            display: flex;
            height: 2rem;
            gap: 0.8rem;
            display: inline-block;
            padding-top:0.5rem;
            padding-right: 0.2rem;
            margin-bottom: -0.5rem;
        }
    .bottlelogo{
            display: flex;
            height: 2rem;
            gap: 0.8rem;
            display: inline-block;
            padding-top:0.4rem;
            margin-bottom: -0.5rem;
            padding-right: 0.2rem;
        }
    .menulogo{
            /* background-color: ; */
            display: flex;
            height: 1.4rem;
            gap: 0.8rem;
            display: inline-block;
            padding-top:0.5rem;
            margin-bottom: -0.3rem;
            padding-right: 0.5rem;
            padding-left: 0.3rem;

        }
    .containerTable-button{
        background: var(--color-tertiary);
        color: var(--color-white);
        font-weight: 600;
        font-family: 'arial', sans-serif;
        padding: 1rem;
        height: 3rem;
        width: 10rem;
        border: none;
        text-transform: uppercase;
        border-radius: 0 15px 15px 0;
        font-size: .7rem;
        margin: 1px;
    }
    .containerTable-button:focus{
        background-color: var(--color-blue-button);
        color: var(--color-white);
    }
    .bottleTable-button{
        background: var(--color-tertiary);
        color: var(--color-white);
        font-weight: 600;
        font-family: 'arial', sans-serif;
        padding: 1rem;
        height: 3rem;
        width: 10rem;
        border-radius: 0 15px 15px 0;
        border: none;
        text-transform: uppercase;
        font-size: .7rem;
        margin: 1px;
    }
    .bottleTable-button:focus{
        background-color: var(--color-blue-button);
        color: var(--color-white);
    }
    .othersTable-button{
        background: var(--color-tertiary);
        color: var(--color-white);
        font-weight: 600;
        font-family: 'arial', sans-serif;
        padding: 1rem;
        height: 3rem;
        width: 10rem;
        border: none;
        text-transform: uppercase;
        font-size: .7rem;
        border-radius: 0 15px 15px 0;
        margin-left: 1px;
        margin-right: 1px;
        margin-bottom: 1px;
        margin-top: 2.5px;
    }
    .othersTable-button:focus{
        background-color: var(--color-blue-button);
        color: var(--color-white);
    }
    .form1-table{
        background-color: var(--color-white);
        padding: 1rem;
        width:40%;
        overflow:auto;
        display: inline-block;
        height: 15rem;
        margin-left: 1rem;
        margin-right: 1rem;
        /* text-align: right; */
        /* display: flex; */
        border: 1px solid var(--color-tertiary);
        position: absolute;
        border-radius: 10px;
        /* box-shadow: 0.5rem 0.5rem 0.5rem var(--color-solid-gray); */
    }
    .textBox-table{
        width:2rem;
        border-radius: 5px;
    }
    .removeBtn{
        background: none;
        border: none;
        font-weight: 600;
        color: var(--color-maroon);
        font-family: 'COCOGOOSE', sans-serif;
    }
    .removeBtn:hover{
        background: var(--color-maroon);
        border: none;
        font-weight: 600;
        border-radius: 15px;
        color: var(--color-white);
    }
    .select-order{
        width: 36rem;
        font-weight: 900;
        height: 2rem;
        background: var(--color-secondary-main);
        color: var(--color-tertiary);
        border: none;
        font-family: 'outfit', sans-serif;
        box-shadow: 0px 0px 2px 1px var(--color-tertiary);
        border-radius: 15px;
    }
    .select-order:hover{
        background: var(--color-main);
        color: var(--color-white);
    }
    .form1-buttons{
        margin-top: 6rem;
        /* margin-bottom: 1rem; */
        margin-left: 30rem;
        align-items: right;
        background-color: none;
        position: relative;
        padding: 1rem;
        width: 100%;
    }
    .addDeliveryFee-button{
        background-color: var(--color-white);
        padding: 0rem;
        height: 3rem;
        width: 14rem;
        font-size: 1rem;
        border-radius: 50px;
        font-weight: 600;
        font-family: 'outfit', sans-serif;
        text-transform: uppercase;
        border-color: var(--color-lightest-gray);
        box-shadow: 0px 1px 0px 0px var(--color-shadow-shadow);
        margin: 1rem;
    }
    .addOrder-button{
        background-color: var(--color-white);
        padding: 0rem;
        height: 3rem;
        width: 14rem;
        font-size: 1rem;
        border-radius: 50px;
        font-weight: 600;
        text-transform: uppercase;
        font-family: 'outfit', sans-serif;
        border-color: var(--color-lightest-gray);
        box-shadow: 0px 1px 0px 0px var(--color-shadow-shadow);
    }

    /* PAYMENT OPTIONS ------------------------------------------------------------------------------ */

    .payment-options{
        /* position:relative; */
        margin-left: -30rem; 
        /* padding: 5px; */
        background-color: none;
        /* display: inline-block; */
        /* align-items: center; */
        position: absolute;
        margin-top: 7rem;
    }
    .paymentOptions-text{
        font-size: 1rem;
        margin-left: -10rem; 
        color: var(--color-solid-gray);
        font-weight: bold;
        display: inline-block;
        align-items: center;
        margin-top: .5rem;
        /* width: 10rem; */
    }
    .paymentOptions-dropdown{
        background-color: var(--color-background);
        margin-right: 5rem;
        /* padding: 0.5rem; */
        display: inline-block;
        align-items: center;
        width: 10rem;
        height: 2rem;
        /* margin-bottom: 1rem; */
        float: right;
    }

    /* RECEIPT --------------------------------------------------------------------------------------*/

    .totalOrder{
        background-color: var(--color-white);
        border-color: var(--color-table-border); 
        width: 25%;
        border-radius: 20px;
        height: 34.3rem;
        /* padding: 4px; */
        position: absolute;
        margin-top: 10.6rem;
        /* display: grid; */
    }
    .company-name{
        color: var(--color-solid-gray);
        font-size: 0.8rem;
        font-weight: bold;
        text-align: center;
        margin-top: .8rem;
        padding-top: 1.2rem;
        text-transform: uppercase;
    }
    .date-Text{
        font-weight: 900;
        font-size: 13px;
        margin-left: 1rem;
        margin-top:1.7rem;
        color: var(--color-black);
        display: inline-block;
    }
    .card {
        display: inline-block;
        /* padding-bottom: 1rem; */
        /* padding-bottom: 1.6rem; */
        /* box-shadow: 0 3px 6px rgba(0, 0, 0, 0.15), 0 3px 6px rgba(0, 0, 0, 0.2); */
        border-radius: 0.1rem;
        height: 1rem;
        margin-left: 2rem;
        border: transparent;
        font-size: 10px;
        font-family: 'Rajdhani', sans-serif;
        /* left: 25vw;      */
        /* margin-top: rem; */
        /* position: absolute; */
    }
    .time{
        /* background-color: var(--color-black); */
        color: var(--color-tertiary);
        font-size: 1.5rem;
        font-weight: 500;
        display: inline-block;
    }
    .date {
        color: var(--color-tertiary);
        font-size: 1.5rem;
        font-weight: 500;
        display: inline-block;
    }
    .dash{
        display: inline-block;
        color: var(--color-tertiary);
    }
    .hr1{
        margin-top: -1rem;
        margin-bottom: -1rem;
    }
    .orderSummary-text{
        color: var(--color-solid-gray);
        font-weight: bolder;
        margin-left: 1rem;
        font-size: 1.4rem;
        text-align: left;
        font-family: 'rajdhani', sans-serif;
        text-transform: uppercase;
        display: inline-block;
        /* position: absolute; */
    }
    .cashier-text{
        color: var(--color-black);
        font-weight: lighter;
        display: inline-block;
        margin-top: 1rem;
        margin-left: 8rem;
        font-size: .8rem;
        text-align: 
    }
    .name-cashier{
        display: inline-block;
        margin-left: 1rem;
        margin-bottom: -1rem;
    }
    .orderSum-table{
        background-color: var(--color-white);
        padding: 1rem;
        width:85%;
        overflow:auto;
        /* display: inline-block; */
        margin-left: 1.1rem;
        height: 13rem;
        margin-top: -1.76rem;
        /* text-align: right; */
        /* display: flex; */
        border-top: 2px solid var(--color-tertiary);
        position: relative;
        border-radius: 10px;
    }
    .tableCheckout table{
        background: var(--color-white);
        font-family: 'Switzer', sans-serif;
        width: 100%;
        font-size: 0.8rem;
        border-radius: 0px 0px 10px 10px;
        padding-left: 0.5rem;
        padding-right: 0.5rem;
        padding-bottom: 2.5rem;
        text-align: center; 
        transition: all 700ms ease;
        overflow: auto;
        margin-top: -1rem;
    }

    table tbody td{
        height: 2.8rem;
        border-bottom: 1px solid var(--color-solid-gray);
        color: var(--color-td); 
        font-size: .67rem;
    }
    .tableCheckout th{
        height: 1.8rem;
        color: var(--color-tertiary);
        margin:1rem;
        font-size: .8rem;
        letter-spacing: 0.02rem;
        border: none;
        /* border-bottom: 2px solid var(--color-solid-gray); */
    }  
    tr:hover td{
        color: var(--color-main); 
        cursor: pointer;
        background-color: var(--color-table-hover);
     } 
    .orderTotal-text{
        color: var(--color-black);
        font-weight: bolder;
        margin-top: 1rem;
        margin-left: 1rem;
        font-size: 1rem;
        align-items: right;
        display: inline-block;
    }
    .peso-sign{
        margin-left: 9rem;
        display: inline-block;
    }
    .total-order{
        display: inline-block;
        border: none;
        background-color: #FFCFCF;
        padding: 7px;
        font-family: 'Calibri', sans-serif;
        font-size: 20px;
        font-weight: 900;
        text-align: right;
        width: 10rem;
        color: var(--color-tertiary);
    }
    .receipt-buttons{
        text-align: center;
        margin-top: 1.3rem;
    }
    .cancelOrder-button{
        background: #c44242;
        /* padding: 1rem; */
        border-radius: 5rem;
        font-size: min(max(9px, 1.1vw), 11px);
        color: white;
        border: none;
        height: 2rem;
        width: 10rem;
        /* position: absolute; */
        margin-left: 1rem;
        font-family: 'COCOGOOSE', sans-serif;
        text-transform: uppercase;
        cursor: pointer; 
   }
    .confirmOrder-button{
        background: var(--color-tertiary);
        border-radius: 5rem;
        font-size: min(max(9px, 1.1vw), 11px);
        color: white;
        border: none;
        height: 2.3rem;
        width: 15rem;
        margin-left: 1rem;
        font-family: 'COCOGOOSE', sans-serif;
        text-transform: uppercase;
        cursor: pointer; 
        
    }
    .confirmOrder-button:hover{
        filter: brightness(120%);
        background: var(--color-tertiary);
        transition: 0.5s; 
    }
    .cancelOrder-button:hover{
        background-color: rgb(158, 0, 0);
        transition: 0.5s; 
    }
    #cancel{
        font-family: 'COCOGOOSE', sans-serif;
        padding: 10px;
        padding-left: 60px;
        padding-right: 60px;
        text-align: center;
        width: 10rem;
        height: 2rem;
        outline: none;
        border: none;
        font-size: min(max(9px, 1.1vw), 11px);
        border-radius: 20px;
        color: white;
        background: #c44242;
        cursor: pointer; 
        transition: 0.5s;
    }
    #cancel:hover{
        background-color: rgb(158, 0, 0);
        transition: 0.5s; 
    }

    /* PREVIOUS TRANSACTIONS ------------------------------------------------------------------------------- */

    .previous-transaction{
        background-color: var(--color-white);
        padding-left: 3rem;
        padding-right: 3rem;
        width: 76.8%;
        height: 40rem;
        margin-left: 21.4rem;
        margin-top: 42rem;
        margin-bottom: 4rem;
        border: 1px solid;
        border-color: var(--color-table-border);
        position: absolute;
        overflow:auto;
        /* position: absolute; */
        /* box-shadow: 0px 5px 30px 2px var(--color-table-shadow); */
        border-top: 8px solid var(--color-table-hover);
        border-radius: 0px 0px 20px 20px;
    }
    .previous-transaction-header{
        color: var(--color-black);
        font-weight: bold;
        font-size: 1rem;
        text-align: center;
        margin-left: 1rem;
        text-transform: uppercase;
        font-family: 'COCOGOOSE', sans-serif;
        letter-spacing: 1px;
        /* margin-bottom:1.5rem; */
    }
    .previous-transaction-table table tbody td{
        height: 3.3rem;
        border-bottom: 1px solid var(--color-border-bottom);
        color: var(--color-td); 
        font-size: .8rem;
    }
    .previous-transaction-table th{
        height: 3.3rem;
        /* padding: 1rem; */
        color: var(--color-black); 
        margin:1rem;
        font-size: .8rem;
        letter-spacing: 0.02rem;
        border: none;
    }  
    .previous-transaction-table tr:hover td{
        color: var(--color-main); 
        cursor: pointer;
        background-color: var(--color-table-hover);
     }
</style>
<!-- SCRIPT------------------------------------------------------------------------------------ -->
<script>

</script>